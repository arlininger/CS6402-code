#!/usr/bin/python

import re
import mysql.connector
from mysql.connector import errorcode
import pgpdb

__doc__ = """
Builds a list of PGP keys and uploads them to a database.
"""

class DBConnection():

#    def __init__(self):
#        try:
#            self.cnx = mysql.connector.connect(
#                user='scriptUser',
#                password='script',
#                host='www.arlininger.com',
#                database='adam_test'
#                )
#            self.cursor = self.cnx.cursor()
#        except mysql.connector.Error as err:
#            print err

    def startBulkUpload(self):
        self.query = "LOCK TABLES `PublicKey` WRITE; "
        self.query += "INSERT INTO `PublicKey` VALUES "
        print "LOCK TABLES `PublicKey` WRITE; "
        print "INSERT INTO `PublicKey` VALUES "

    def appendKey(self, key):
        self.query += "('%s','%s','%s',NULL,NULL,NULL,NULL)," % (key['fingerprint'], key['key_id'], key['create_date'])
        print "('%s','%s','%s',NULL,NULL,NULL,NULL)," % (key['fingerprint'], key['key_id'], key['create_date'])

    def bulkUpload(self):
        self.query = self.query.rstrip(',')
        self.query += "; UNLOCK TABLES;"
        print "; UNLOCK TABLES;"
#        self.cursor.execute(self.query, multi=True)
#        self.cnx.commit()
#        print self.query

    def flushTables(self):
#        truncate = "TRUNCATE TABLE PublicKey"
#        self.cursor.execute(truncate)
#        self.cnx.commit()
#        print "Fingerprint, KeyID, CreationDate"
        pass
        
    def close(self):
#        self.cursor.close()
#        self.cnx.close()
        pass

    def upload_key(self, key):
        try:
            add_pub_key = ("INSERT INTO PublicKey(Fingerprint, KeyID, CreationDate) VALUES ('%s','%s','%s')" %
                (
                    key['fingerprint'],
                    key['key_id'],
                    key['create_date'],
                )
            )
#            self.cursor.execute(add_pub_key)
#            self.cnx.commit()
            print ("%s, %s, %s" %
                (
                    key['fingerprint'],
                    key['key_id'],
                    key['create_date'],
                )
            )
        except Exception as e:
            print e
#            import pdb
#            pdb.set_trace()
    
def main():
#    records = pgpdb.pgprecords("shortset")
    records = pgpdb.pgprecords()
    records.parse()
#    print "Done Parsing"
    db = DBConnection()
#    db.flushTables()
    db.startBulkUpload()
    for key in records.keys:
        db.appendKey(key)
#        count = count + 1
#        db.upload_key(key)
#        if count % 10000 == 0:
#            print count
#    print "About to upload"
    db.bulkUpload()
    db.close()

if __name__ == '__main__':
    main()


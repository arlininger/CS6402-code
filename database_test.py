#!/usr/bin/python

import mysql.connector
import sys

def main():
	try:
		cnx = mysql.connector.connect(user='scriptUser', password='script',
			host='www.arlininger.com', database='adam_test')
		cursor = cnx.cursor()
		add_pub_key = ("INSERT INTO PublicKey(Fingerprint, KeyID, CreationDate) VALUES ('%s','%s','%s')" %
			('ABCDEF0123456789ABCDEF0123456789ABCDEF02','23456789ABCDEF01','1970-01-01'))
		cursor.execute(add_pub_key)
		cnx.commit()
		cursor.close()
		cnx.close()
#		import pdb; pdb.set_trace()
	except Exception as e:
		print e

main()

#!/usr/bin/python

import re

from graph_tool.all import Graph

__doc__ = """
This module is used for working with a dump of pgp keys.
"""

pubkey = re.compile(
    r'^pub:' +
    r'.*:' +
    r'(?P<size>-?\d*):' +
    r'.*:' +
    r'(?P<keyid>[0-9A-F]{16}):' +
    r'(?P<create_date>\d\d\d\d-\d\d-\d\d):' +
    r'(?P<expire_date>(\d\d\d\d-\d\d-\d\d)?):' +
    r'.*:'
)

fingerprint = re.compile(
    r'^fpr:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'(?P<fingerprint>[0-9A-F]{40}):'
)

userid = re.compile(
    r'^uid:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'.*:' +
    r'(?P<uid>(?P<name>[^:]*?)( \(((?P<comment> \(.*\))?.*)\))?( <(?P<username>.*)@(?P<domain>[a-zA-Z0-9.]*)>)?):' +
    r''
)

signature = re.compile(
    r'^sig:' +
    r'(?P<keyid>[0-9A-F]{16}):' +
    r'(?P<sign_date>\d\d\d\d-\d\d-\d\d):' +
    r'.*'
)

subkey = re.compile(
    r'^sub:' +
    r'.*'
)

revocation = re.compile(
    r'^rev:' +
    r'.*'
)

class pgprecords():
	
	def __init__(self, filename=None):
		self.keys = list()
		if filename:
			self.filename = filename
		else:
			self.filename = 'preprocessedData'

	def parse(self):
	    key = dict()
	    with open(self.filename) as f:
	        for line in f:
	            line = line.rstrip('\r\n ')
	
	            match = pubkey.search(line)
	            if (match):
	                if key:
	                    self.keys.append(key)
	                key = dict()
	                key['key_id'] = match.group('keyid')
	                key['create_date'] = match.group('create_date')
	                key['expire_date'] = match.group('expire_date')
	                key['fingerprint'] = None
	                continue
	
	            match = fingerprint.search(line)
	            if match:
	                key['fingerprint'] = match.group('fingerprint')
	                continue
	
	            match = userid.search(line)
	            if match:
	                if not 'userid' in key:
	                    key['userid'] = list()
	                key['userid'].append({
	                    'name': match.group('name'),
	                    'comment': match.group('comment'),
	                    'username': match.group('username'),
	                    'domain': match.group('domain'),
	                    'uid': match.group('uid')
	                })
	                continue
	
	            match = signature.search(line)
	            if match:
	                if not 'signatures' in key:
	                    key['signatures'] = list()
	                key['signatures'].append({
	                    'keyid': match.group('keyid'),
	                    'sign_date': match.group('sign_date')
	                })
	                continue
	
	            match = subkey.search(line)
	            if match:
	                continue
	
	            match = revocation.search(line)
	            if match:
	                continue
	
	            print line

if __name__ == '__main__':
	#records = pgprecords('shortset')
	records = pgprecords()
	records.parse()
	#print records.keys
	print "Done parsing records"
	g = Graph()
	keys = dict() #key is 64-bit ID, value is Graph Vertex
	vlabel = g.new_vertex_property("string")
	vid = g.new_vertex_property("string")
	vuid = g.new_vertex_property("string")
	

	for key in records.keys:
		if key['key_id'] in keys:
			v = keys[key['key_id']]
		else:
			v = g.add_vertex()
			keys[key['key_id']] = v
		vlabel[v] = key['userid'][0]['domain'] or "NONE"
		vuid[v] = key['userid'][0]['uid'] or "NONE"
		vid[v] = key['key_id']
		try:
			signatures = key['signatures']
			for signature in signatures:
				if not signature['keyid'] in keys:
					v_sig = g.add_vertex()
					keys[signature['keyid']] = v_sig
					vid[v_sig] = signature['keyid']
				g.add_edge(keys[signature['keyid']],v)
		except KeyError as e:
			pass
	g.vp["label"] = vlabel
	g.vp["id"] = vid
	g.vp["uid"] = vuid
	#g.save("pgp_short_graph.dot",fmt="dot")
	#g.save("pgp_graph.dot",fmt="dot")
	#g.save("pgp_short_graph.data", fmt="gt")
	g.save("pgp_graph.data", fmt="gt")
        
        
        
        
        
        
        
        
        


from graph_tool.all import *
import operator
from datetime import datetime
import sys

import traceback
from multiprocessing.pool import Pool
from multiprocessing import Process, Queue
import multiprocessing
import logging

logger = multiprocessing.log_to_stderr()
logger.setLevel(logging.WARNING)

MINIMUM_SUPPORT = 5
SIZE_LIMIT = 5
THREAD_COUNT = 12

# Shortcut to multiprocessing's logger
def error(msg, *args):
    return multiprocessing.get_logger().error(msg, *args)

class LogExceptions(object):
    def __init__(self, callable):
        self.__callable = callable
        return

    def __call__(self, *args, **kwargs):
        try:
            result = self.__callable(*args, **kwargs)

        except Exception as e:
            # Here we add some debugging help. If multiprocessing's
            # debugging is on, it will arrange to log the traceback
            error(traceback.format_exc())
            # Re-raise the original exception so the Pool worker can
            # clean up
            raise

        # It was fine, give a normal answer
        return result
    pass

class LoggingPool(Pool):
    def apply_async(self, func, args=(), kwds={}, callback=None):
        return Pool.apply_async(self, LogExceptions(func), args, kwds, callback)

def print_graph(graph, fh=sys.stdout):
    vlabel = graph.vertex_properties["label"]
    for v in graph.vertices():
        print >> fh,"%d: %s" % (graph.vertex_index[v], vlabel[v])
    for e in graph.edges():
        print >> fh,"    %d => %d" %(graph.vertex_index[e.source()],graph.vertex_index[e.target()])
    print >> fh,""
        
def generate_graphs(graph):
    for v1 in graph.vertices():
        for v2 in graph.vertices():
            if (not graph.edge(v1,v2) and
                not v1 == v2):
                ret = Graph(graph)
                ret.add_edge(ret.vertex(graph.vertex_index[v1]),ret.vertex(graph.vertex_index[v2]))
                yield ret
    maxlabel = 0
    for v in graph.vertices():
        if graph.vertex_properties["label"][v] > maxlabel:
            maxlabel = graph.vp["label"][v]
    for v in graph.vertices():
        for I in xrange(maxlabel+2): # to increase possible labels by 1
            ret = Graph(graph)
            new_v = ret.add_vertex()
            ret.vp["label"][new_v] = I
            ret.add_edge(ret.vertex(graph.vertex_index[v]),new_v)
            yield ret
            ret = Graph(graph)
            new_v = ret.add_vertex()
            ret.vp["label"][new_v] = I
            ret.add_edge(new_v,ret.vertex(graph.vertex_index[v]))
            yield ret

def hasSupport(subgraph, graph, min_support):
    # Filter things that can't even be a subgraph
    matchcount = 0;
    subv = list(subgraph.vertices())
    sube = list(subgraph.edges())
    gv = list(graph.vertices())
    ge = list(graph.edges())
    
    if (not len(subv) <= len(gv) or
        not len(sube) <= len(ge)):
        logger.info("Support Returning False")
        return False
    vp_sub = subgraph.vp['label']
    subgraph_map = dict()
    graph_map = dict()
    for v in subv:
        if vp_sub[v] in subgraph_map:
            subgraph_map[vp_sub[v]] += 1
        else:
            subgraph_map[vp_sub[v]] = 1

    # count instances of each key
    vp_g = graph.vp['label']
    for v in gv:
        if vp_g[v] in graph_map:
            graph_map[vp_g[v]] += 1
        else:
            graph_map[vp_g[v]] = 1

    # If the subgraph has more labels than the graph, can't have support
    # If the subgraph has a label that occurs more often than any one in
    # the graph, can't have support
    if len(subgraph_map) > len(graph_map):
        logger.info("Support Returning False")
        return False
    if max(subgraph_map.values()) > max(graph_map.values()):
        logger.info("Support Returning False")
        return False

    # This call is very expensive. Try to filter as we can before hand
    mappings = graph_tool.topology.subgraph_isomorphism(subgraph, graph)
    for mapping in mappings:
        mapcheck = dict()
        match = True
        for v in subv:
            if not vp_sub[v] in mapcheck:
                mapcheck[vp_sub[v]] = vp_g[graph.vertex(mapping[v])]
            elif mapcheck[vp_sub[v]] != vp_g[graph.vertex(mapping[v])]:
                match = False
                break
        if match:
            matchcount += 1
            if matchcount >= min_support:
                logger.info("Support Returning True")
                return True
    logger.info("Support Returning False")
    return False

def isEquivelant(graph1,graph2):
    g1v = list(graph1.vertices())
    g2v = list(graph2.vertices())
    g1e = list(graph1.edges())
    g2e = list(graph2.edges())
    vp1 = graph1.vp['label']
    vp2 = graph2.vp['label']
    g1_map = dict()
    g2_map = dict()
    
    #Build dict of labels for both graphs
    for v in graph1.vertices():
        if vp1[v] in g1_map:
            g1_map[vp1[v]] += 1
        else:
            g1_map[vp1[v]] = 1
    for v in graph2.vertices():
        if vp2[v] in g2_map:
            g2_map[vp2[v]] += 1
        else:
            g2_map[vp2[v]] = 1
    g1_list = sorted(g1_map.items(), key=operator.itemgetter(1))
    g2_list = sorted(g2_map.items(), key=operator.itemgetter(1))
    if len(g1_list) != len(g2_list):
        return False
        
    for I in xrange(len(g1_list)):
        if g1_list[I] != g2_list[I]:
            return False
    if (not len(g1v) == len(g2v) or
        not len(g1e) == len(g2e)):
        return False

    for mapping in graph_tool.topology.subgraph_isomorphism(graph1,graph2):
        mapcheck1 = dict()
        mapcheck2 = dict()
        match = True
        for v1 in g1v:
            v2 = graph2.vertex(mapping[v1])
            if not vp1[v1] in mapcheck1.keys():
                mapcheck1[vp1[v1]] = vp2[v2]
            elif mapcheck1[vp1[v1]] != vp2[v2]:
                match = False
                break
            if not vp2[v2] in mapcheck2.keys():
                mapcheck2[vp2[v2]] = vp1[v1]
            elif mapcheck2[vp2[v2]] != vp1[v1]:
                match = False
                break
        if match:
            return True
    return False

def isGraphInSet((graph,graphset)):
    for g in graphset:
        if isEquivelant(graph,g):
            #q.put(True)
#            return True
            return (graph,True)
    #q.put(False)
#    return False
    return (graph,False)

def worker(q,returnq):
    print "starting worker"
    listOfGraphs = list()
    while True:
        data = q.get()
        if not data: break
        logger.info(data)
        if data == "die": break
        elif data == "newGraphInList":
            data = q.get()
            listOfGraphs.append(data)
        elif data == "graphToCheck":
            data = q.get()
            result = isGraphInSet((data,listOfGraphs))
            returnq.put(result)
        elif data == "reset":
            listOfGraphs = list()
        else:
            logging.warning("Got unknown command %s" % data)

def algorithm(data):
    
    threadcount = THREAD_COUNT
    p = list()
    for I in range(threadcount):
        q = Queue()
        returnq = Queue()
        proc = Process(target=worker, args=(q,returnq,))
        proc.start()
        p.append({'q': q, 'returnq': returnq, 'proc': proc})

    # Set up basic data set
    # The basic data set includes only two possible graphs.
    # A graph of size 1 (edge) where both end points have the same domain in the email address
    # A graph of size 1 (edge) where endpoints have different domains
    subgraphs = [Graph()]
    subgraphs[0].add_vertex(2)
    
    subgraphs[0].add_edge(subgraphs[0].vertex(0),subgraphs[0].vertex(1))
    subgraphs.append(Graph(subgraphs[0]))
    
    vlabel = subgraphs[0].new_vertex_property("int")
    vlabel[subgraphs[0].vertex(0)] = 0
    vlabel[subgraphs[0].vertex(1)] = 0
    subgraphs[0].vp["label"] = vlabel

    vlabel = subgraphs[1].new_vertex_property("int")
    vlabel[subgraphs[1].vertex(0)] = 0
    vlabel[subgraphs[1].vertex(1)] = 1
    subgraphs[1].vp["label"] = vlabel
    deadEndGraphs = list()

    haveSupport = True
    size = 1
    print "Starting Algorithm"
    lasttime = datetime.now()
    while haveSupport:
        haveSupport = False
        graphsToCheck = list()
        for subgraph in subgraphs:
            deadEnd = True
            processList = dict()
            newgraphs = list()
            subgraphList = list(generate_graphs(subgraph))
#            newgraphs = map(None,subgraphList,[graphsToCheck] * len(subgraphList))
            isEquivelantList = list()
            nextIndex = 0
            for subgraph in subgraphList:
                p[nextIndex]['q'].put("graphToCheck")
                p[nextIndex]['q'].put(subgraph)
                nextIndex += 1    
                if nextIndex >= threadcount:
                    for I in range(threadcount):
                        result = p[I]['returnq'].get()
                        isEquivelantList.append(result)
                    nextIndex = 0
            for I in range(nextIndex):
                result = p[I]['returnq'].get()
                isEquivelantList.append(result)
            for newgraph,equivelant in isEquivelantList:
                if (not equivelant and
                    hasSupport(newgraph,data,MINIMUM_SUPPORT)):
                    haveSupport = True
                    deadEnd = False
                    graphsToCheck.append(newgraph)
                    for proc in p:
                        proc['q'].put("newGraphInList")
                        proc['q'].put(newgraph)
            if deadEnd:
                deadEndGraphs.append(subgraph)
        subgraphs = graphsToCheck
        graphsToCheck = list()
        for proc in p:
            proc['q'].put("reset")
        delta = datetime.now() - lasttime
        h, rem = divmod(delta.seconds, 3600)
        m, s = divmod(rem, 60)
        print "%d\t%d:%02d:%02d" % (size, h, m, s)
        lasttime = datetime.now()
        size += 1
        if size >= SIZE_LIMIT:
            break

    print "\n"
    with open("graph_output.txt","w") as f:
        for mygraph in subgraphs:
            print_graph(mygraph, f)
    print len(deadEndGraphs)
    for proc in p:
        proc['q'].put("die")

if __name__ == '__main__':
    pass

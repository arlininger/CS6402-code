#!/usr/bin/env python
#from graph import graph
from graph_tool.all import *
from itertools import izip
from numpy.random import randint

import apriori
import scoring

import yappi

def main():
    g = load_graph("my_test_graph.dot")
    apriori.algorithm(g)
    
if __name__ == '__main__':
    yappi.start(builtins=False,profile_threads=True)
    main()
    funcstats = yappi.get_func_stats()
    funcstats.save("./apriori.out.1234","callgrind")
    


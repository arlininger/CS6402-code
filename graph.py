
#This is intended to hold a graph class that allows nodes and edges to be
#represented. The intent is that all graph helper functions (like comparision)
#should be implemented here.

from copy import copy,deepcopy
import sys

class graph:

    def __init__(self, **kwargs):
        self.nodes = list()
        self.edges = list()
        
        if 'graph' in kwargs:
            self.nodes = copy(kwargs['graph'].nodes)
            self.edges = copy(kwargs['graph'].edges)

        if ('nodes' in kwargs and
            'edges' in kwargs):
            self.nodes = copy(kwargs['nodes'])
            self.edges = copy(kwargs['edges'])

    def addNode(self, node):
        self.nodes.append(node)
    
    def addEdge(self, edge):
        if (edge['head'] in self.nodes and
            edge['tail'] in self.nodes):
            self.edges.append(edge)

    def output(self, filehandle=None):
        if filehandle is None:
            filehandle = sys.stdout

        print >> filehandle, "digraph {"
        for node in self.nodes:
            print >> filehandle, "    \"%s\"[label=\"%s\"];" % (node['uniqueid'], node['label'])
        for edge in self.edges:
            print >> filehandle, "    \"%s\" -> \"%s\";" %(edge['tail']['uniqueid'], edge['head']['uniqueid'])

        print >> filehandle, "}"

    def generate(self):
        #Generate all possible extensions of this graph created by adding one edge
        #Edge may be added to an existing or a new node
        #If a new node, edge may be an existing or new label
        maxlabel = 0
        maxid = 0
        for node in self.nodes:
            if node['label'] > maxlabel:
                maxlabel = node['label']
            if node['uniqueid'] > maxid:
                maxid = node['uniqueid']

        for node in self.nodes:
            for secondnode in self.nodes:
                if not {'head':node,'tail':secondnode} in self.edges:
                    #What we really want is a deep copy of the graph...and then add the edge
                    tempedges = deepcopy(self.edges)
                    tempedges.append({'head':node,'tail':secondnode})
                    yield graph(nodes=self.nodes, edges=tempedges)
            for I in xrange(maxlabel + 1): #all existing labels and one new one
                newgraph = deepcopy(self)
                tempnode = {"uniqueid": maxid+1, "label": I}
                newgraph.addNode(tempnode)
                tempedge = {'head':node,'tail':tempnode}
                newgraph.addEdge(tempedge)
                yield newgraph
                newgraph = deepcopy(self)
                tempnode = {"uniqueid": maxid+1, "label": I}
                newgraph.addNode(tempnode)
                tempedge = {'head':tempnode,'tail':node}
                newgraph.addEdge(tempedge)
                yield newgraph


    def isSubGraph(self, graph):
        if graph == self: return True
        
        
    def hasSupport(self, graph, support):
        if len(graph.nodes) <= 10:
            return True
        return False



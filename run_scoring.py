#!/usr/bin/env python
#from graph import graph
from graph_tool.all import *
from itertools import izip
from numpy.random import randint

import apriori
import scoring

import yappi

def main():
    #g = load_graph("pgp_short_graph.dot")
    #g = load_graph("pgp_graph.dot")
    #g = load_graph("pgp_short_graph.data", fmt="gt")
    g = load_graph("pgp_graph.data", fmt="gt")
    scoring.algorithm(g)
    
if __name__ == '__main__':
    yappi.start(builtins=False,profile_threads=True)
    main()
    funcstats = yappi.get_func_stats()
    funcstats.save("./pgpdb.out.1234","callgrind")
    


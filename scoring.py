
from graph_tool.all import *
from collections import defaultdict

import operator

def algorithm(g):
	labels = g.vp['label']
	ids = g.vp['id']
	uids = g.vp['uid']
	scores = defaultdict(int)
	for e in g.edges():
		if labels[e.source()] == labels[e.target()]:
			modifier = 1
		else:
			modifier = 2
		srcindex = g.vertex_index[e.source()]
		tgtindex = g.vertex_index[e.target()]
		#srcindex = ids[e.source()]
		#tgtindex = ids[e.target()]
		scores[srcindex] = (scores[srcindex] + modifier) or modifier
		scores[tgtindex] = (scores[tgtindex] + modifier*2) or modifier*2

	sorted_scores = sorted(scores.items(), key=operator.itemgetter(1), reverse=True)
	
	print "ID, Score"
	for I in range(1000):
		print "ID: %s Score: %d\tUID: %s" % (
		    ids[g.vertex(sorted_scores[I][0])],
		    sorted_scores[I][1],
		    uids[g.vertex(sorted_scores[I][0])]
		)
	
if __name__ == '__main__':
    pass
